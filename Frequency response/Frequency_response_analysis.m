%% Initialisation

clc;
clearvars -except file_path;
close all;

%% Select file

% Clear `file_path` variable to prompt for a new file
if ~exist('file_path', 'var') || ~any(file_path)
    [file_name, path] = uigetfile('*.txt', 'Select Python log file');
    file_path = [path, file_name];
end

% Pre-set frequencies
omega = [1.0, 2.0, 3.0, 4.0]; % Add lines to indicate multisine frequencies (leave empty for no lines)
% omega = [];

%% Import file

data = readtable(file_path, 'VariableNamingRule', 'preserve');
data.Properties.VariableNames = {'time', 'pwm', 'encoder'};

N = height(data);

t_sample = mean(diff(data.time)); % Get sample time from data

velocity = [0; diff(data.encoder) / t_sample]; % Pad with zero to match original size

%% Make plot

figure;
plot(data.time, data.encoder);
xlabel('Time (s)');
ylabel('Position (counts)');

%% Frequency analysis

Fs = 1 / t_sample;
f = 2 * pi * Fs * (0 : (N/2)) / N;

% Input
Y = fft(data.pwm);
P2 = abs(Y / N);
P1 = P2(1:(N / 2 + 1));
P1(2:end-1) = 2 * P1(2:end-1);
P1 = mag2db(P1);

% Output
Z = fft(data.encoder);
Q2 = abs(Z / N);
Q1 = Q2(1:(N / 2 + 1));
Q1(2:end-1) = 2 * Q1(2:end-1);
Q1 = mag2db(Q1);

% Transfer function
T = Z ./ Y;
R2 = abs(T / N);
R1 = R2(1:(N / 2 + 1));
R1(2:end-1) = 2 * R1(2:end-1);
R1 = mag2db(R1);

%% Make amplitude spectrum plots

% Input
figure;
semilogx(f, P1);
title('Amplitude spectrum of input');
xlabel('\omega (rad/s)');
ylabel('Amplitude (dB)');
grid on;

% Output
figure;
semilogx(f,Q1);
title('Amplitude spectrum of output');
xlabel('\omega (rad/s)');
ylabel('Amplitude (dB)');
grid on;

% Transfer function
figure;
semilogx(f,R1);
hold on
for w = omega
    xline(w);
end
hold off
title('Amplitude spectrum of transfer function');
xlabel('\omega (rad/s)');
ylabel('Amplitude (dB)');
grid on
