"""
Put either a multi-sine or a chirp (increasing frequency) through the motor.
"""

from robot_freq import RobotFreq


robot = RobotFreq(100.0)

robot.ticker.start()
