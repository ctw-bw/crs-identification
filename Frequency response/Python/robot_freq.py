from math import sin
from robot_base import Robot


class RobotFreq(Robot):
    """Extended Robot class for frequency response.

    Reply on parent class for all state logic.
    """

    MODE_CHIRP = False  # Set to `False` for multi-sine, set to `True` for chirp

    MEASURE_TIME = 30  # Measure time in seconds

    SIN_FREQUENCIES = [1.0, 2.0, 3.0, 4.0]  # Sine frequencies in rad/s
    SIN_AMPLITUDES = [0.5] * 4  # Sine amplitudes (in pwm 0.0 - 1.0)

    CHIRP_FREQUENCY = 1.0  # Chirp start frequency in rad/s
    CHIRP_AMPLITUDE = 0.5  # Chirp amplitude (in pwm 0.0 - 1.0)
    CHIRP_BETA = 0.1  # Frequency increment in rad/s per second of elapsed time

    def get_motor_input(self):
        """Get step response motor input."""

        time = self.get_time()

        if self.MODE_CHIRP:  # Create chirp output
            freq = self.CHIRP_FREQUENCY + self.CHIRP_BETA * time
            pwm = self.CHIRP_AMPLITUDE * sin(freq * time)

        else:  # Create multi-sine output
            pwm = 0
            for amp, freq in zip(self.SIN_AMPLITUDES, self.SIN_FREQUENCIES):
                pwm += amp * sin(freq * time)

        return pwm
