from pyb import Switch, millis, Pin
from biorobotics import Ticker, PWM, Encoder, SerialPC


class Robot:
    """Common state machine class."""

    IDLE = "idle"
    ACTIVE = "active"

    MEASURE_TIME = 30  # Measure time in seconds

    def __init__(self, fs):

        self.ticker = Ticker(0, fs, self.run, enable_gc=True)

        self.state = None  # Current state

        self.state_init = None  # True when a new state was just started
        self.state_start = None  # Start time of current state

        self.button = Switch()
        self.pwm = PWM("D5")
        self.dir = Pin("D4", Pin.OUT)
        self.encoder = Encoder("D11", "D12")

        self.file = None  # File handle during write

        self.pc = SerialPC(2)

        # Initialize state
        self.set_state(self.IDLE)

    def set_state(self, new_state):
        """Update the state."""

        if not self.state == new_state:
            self.state_start = millis()
            self.state_init = True
            self.state = new_state

    def get_time(self):
        """Get current state time in seconds."""
        return (millis() - self.state_start) / 1000.0

    def set_motor_pwm(self, value):
        """Set new motor pwm value (between -1.0 and 1.0)."""
        self.dir.value(value > 0)
        self.pwm.write(value)

    def get_motor_input(self):
        """Return motor input during the Active state.

        Override in child class to change behaviour.
        """
        return 0.0

    def run(self):
        """Timer callback."""

        was_init = self.state_init

        # Run the currently selected method
        if self.state == self.IDLE:
            self.idle()
        elif self.state == self.ACTIVE:
            self.active()
        else:
            raise ValueError("Unknown state")

        # Do not reset init if it was only just assigned
        if self.state_init and was_init:
            self.state_init = False

    def idle(self):
        """STATE: Idle"""

        if self.state_init:
            print("State: Idle")

        self.set_motor_pwm(0.0)

        if self.button.value():
            self.set_state(self.ACTIVE)

    def active(self):
        """STATE: Active

        This base method will make sure the log file is opened and closed, and
        the state is left automatically after self.MEASURE_TIME seconds.
        """

        if self.state_init:
            print("State: Active")

        if self.file is None:
            self.file = open("output.txt", "w+")
            self.file.write("time [s];pwm;encoder [counts]")

        pwm = self.get_motor_input()

        self.set_motor_pwm(pwm)

        self.file.write(
            "%.4f;%.4f;%d\n" % (self.get_time(), pwm, self.encoder.counter())
        )

        self.pc.set(0, pwm)
        self.pc.set(1, self.encoder.counter())
        self.pc.send()

        if self.get_time() > self.MEASURE_TIME:
            self.set_state(self.IDLE)

        # If on the way out
        if not self.state == self.ACTIVE:

            self.ticker.stop()

            self.file.close()
            self.file = None

            # Closing the file will take very long, overloading the ticker.
            # So we just stop and start it again.

            self.ticker.start()
