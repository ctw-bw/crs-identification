"""
Put a step response to the motor and log the output.
"""

from robot_step import RobotStep


robot = RobotStep(100.0)

robot.ticker.start()
