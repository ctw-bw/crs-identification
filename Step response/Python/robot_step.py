from robot_base import Robot


class RobotStep(Robot):
    """Extended Robot class for step response.

    Reply on parent class for all state logic.
    """

    MEASURE_TIME = 1.0  # Measure time in seconds

    def get_motor_input(self):
        """Get step response motor input."""

        pwm = 0.0 if self.get_time() < 0.1 else 1.0

        return pwm
