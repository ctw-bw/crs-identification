%% Initialisation

clc;
clearvars -except file_path;
close all;

%% Select file

% Clear `file_path` variable to prompt for a new file
if ~exist('file_path', 'var') || ~any(file_path)
    [file_name, path] = uigetfile('*.txt', 'Select Python log file');
    file_path = [path, file_name];
end

%% Import file

data = readtable(file_path, 'VariableNamingRule', 'preserve');
data.Properties.VariableNames = {'time', 'pwm', 'encoder'};

t_sample = mean(diff(data.time)); % Get sample time from data

velocity = [0; diff(data.encoder) / t_sample]; % Pad with zero to match original size

%% Make plot

figure;
plot(data.time, velocity);
grid on
title('Velocity as function of time');
xlabel('Time (s)');
ylabel('Velocity [Units/s]');


%% Calculate

% Exercise: calculate g_0, d, J and tau based on the data
g_0 = pi; % <- change this
tau = pi; % <- change this


s = tf('s');
T = g_0 / (tau*s + 1);  % transfer function
om_th = step(T, data.time);     % step response

figure;
plot(data.time, om_th);
title('Step response');
xlabel('Time (s)');
ylabel('Velocity [Units/s]');
