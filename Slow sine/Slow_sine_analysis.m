%% Initialisation

clc;
clearvars -except file_path;
close all;

%% Select file

% Clear `file_path` variable to prompt for a new file
if ~exist('file_path', 'var') || ~any(file_path)
    [file_name, path] = uigetfile('*.txt', 'Select Python log file');
    file_path = [path, file_name];
end

%% Import file

data = readtable(file_path, 'VariableNamingRule', 'preserve');
data.Properties.VariableNames = {'time', 'pwm', 'encoder'};

t_sample = mean(diff(data.time)); % Get sample time from data

velocity = [0; diff(data.encoder) / t_sample]; % Pad with zero to match original size

%% Make plots

figure;
plot(data.time, data.encoder);
xlabel('Time (s)')
ylabel('Position (counts)')

figure;
plot(velocity, data.pwm);
xlabel('Velocity (counts/s)');
ylabel('PWM Duty Cycle');
