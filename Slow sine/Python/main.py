"""
Put a slow sine to the motor and log the output.
"""

from robot_sine import RobotSine


robot = RobotSine(100.0)

robot.ticker.start()
