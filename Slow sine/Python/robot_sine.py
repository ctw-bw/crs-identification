from math import sin
from robot_base import Robot


class RobotSine(Robot):
    """Extended Robot class for sine response.

    Reply on parent class for all state logic.
    """

    MEASURE_TIME = 30.0  # Measure time in seconds
    SIN_FREQUENCY = 0.30  # Sine frequency in rad/s
    SIN_AMPLITUDE = 0.30  # Sine amplitude (in pwm 0.0 - 1.0)

    def get_motor_input(self):
        """Get step response motor input."""

        pwm = self.SIN_AMPLITUDE * sin(self.SIN_FREQUENCY * self.get_time())

        return pwm
