# CRS Identification and Control Practical

This repository contains the code for the CRS practical.

## How To Use

 1. Load the Python files from the `[Experiment]/Python/` directories onto the microcontroller.
 2. Press the blue button to start the experiment.
    * You can use uScope to monitor the input and output 
 3. After the experiment, do a hard reset on the board using the black button.
 4. After reconnecting to your PC, you should see a new file on the Pybflash partition called `output.txt`.
 5. Process the log file using the MATLAB scripts.

*Note:* the instances of `robot_base.py` are identical. They are copied to make flashing the controller easier.
